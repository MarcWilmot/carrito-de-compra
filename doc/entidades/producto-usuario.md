### Producto usuario

Atributo | Tipo | Descripción | Requerido | Único
-- | -- | -- | -- | --
id| INT |Identificador de la relacion usuario-producto | Sí | Sí
id_compra| INT |Identificador de la compra | Sí | No
id_producto| INT |Identificador del producto | Sí | Sí
id_usuario| INT |Identificador del usuario | Sí | Sí
fecha_compra | TIMESTAMP |fecha de la compra del producto | Sí | No
procesado| INT |Este campo únicamente tiene tres estados: 0,1,2, 0 = el cliente aún no ha procesado la compra. 1=El cliente ha procesado la compra y esta está pendient. 2= La compra ya ha sido procesada y enviada por el administrador| Sí | No
