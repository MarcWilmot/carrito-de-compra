  ## Cliente registrado

Cómo cliente registrado podré:

+ Navegar por los productos aplicando diferentes filtros
+ Agregar una cantidad *X* de un determinado producto al carrito
+ Eliminar productos del carrito
+ Ver su histórico de compras
+ Procesar la compra
+ Editar su perfil
+ Login
+ Recuperación de contraseña

---

### Navegar por los productos usando filtros
Como **Cliente** quiero **aplicar filtros a los productos** para **ver fácil y directamente los productos que necesito**

#### Criterios de aceptación:
Dado **que no queda stock del producto ofertado** Cuando **el cliente navega por los productos** Entonces **mostrar un mensaje de AGOTADO**

Dado **que hay muchos productos** Cuando **el cliente filtra los productos** Entonces **mostrar una paginación**

---

### Añadir productos al carrito
Como **Cliente** quiero **agregar más de un producto al carrito a la vez** para **comprar el mismo producto un número varias veces de forma rápida y simple**

#### Criterios de aceptación:
Dado **que el número de productos solicitados es mayor que el disponible en el stock** cuando **compro en la misma acción el mismo producto** entonces **mostrar un mensaje informándo que no puede comprar más de X veces ese producto**

Dado **que ya tenía el producto en el proceso de compra (carrito)** cuando **vuelve a comprar el mismo producto** entonces **aumentar en la vista de carrito el número del producto solicitado, no duplicar el producto**

---
### Eliminar productos del carrito
Como **Cliente**  quiero **eliminar productos añadidos al carrito**  para **modificar mi compra final**
#### Criterios de aceptación:
Dado **que pudiera eliminar un producto por error** cuando **gestiono mi carrito de compra** entonces **mostrar un mensaje de confirmación**

---
### Historial de compras
Como **Cliente**  quiero **ver el historial de mis compras**  para **saber qué he comprado en esta tienda**.
#### Criterios de aceptación:
Dado **que puede haber un historial de compras muy largo** cuando **entro en la vista de mi historial de compras** entonces **mostrar una serie de filtros, fecha, categoria de producto**


---
### Procesar la compra
Como **Cliente**  quiero **procesar la compra**  para **disponer de los productos comprados**.
#### Criterios de aceptación:
Dado **que puedo haberle dado a procesar la compra por error** cuando **gestiono mi carrito** entonces **mostrar un mensaje de confirmación**

---

### Editar perfil
Como **Cliente**  quiero **editar mi perfil de usuario**  para **modificar y actualizar mis datos**.
#### Criterios de aceptación:
Dado **que puedo haber modificado algún dato por error** cuando **estoy modificando mi perfil** entonces **mostrar un mensaje de confirmación**

---
### Login
Como **Cliente**  quiero **loguearme**  para  **acceder al sistema**.
#### Criterios de aceptación:
Dado **que he olvidado la constraseña** cuando **intento acceder al sistema** entonces **ofrecerme la posibilidad de recuperar contraseña**

Dado **que he introducido la contraseña/usuario incorrecto** cuando **intento loguearme** entonces **mostrar un mensaje de error y volver a mostrar el formulario de logueo**

---

#### Menú Cliente
Como **Cliente**  quiero **ver el menú de cliente**  para **acceder a los privilegios cómo cliente**.

---
### Recueprar contraseña
Como **Cliente**  quiero **recuperar mi contraseña**  para **acceder con mi al sistema si olvido la contraseña**.
#### Criterios de aceptación:
Dado **Que la contraseña nueva es la misma que la antigua** cuando **cambio mi contraseña** entonces **mostrar un mensaje de error y solicitar una nueva contraseña**

---
---

#### Criterios de aceptación:
+ **Dado** que el usuario cliente no dispone de todos los privilegios **cuando** intenta acceder a alguna funcionalidad que requiere previlegios avanzados, **entonces** informarle de que no puede acceder por falta de privilegios.
+ **Dado** que el usuario ha introduzido algún dato incorrecto **cuando** intenta logearse en el sistema **entonce** informarle de que algún dato es incorrecto y ofrecerle la oportunidad de volvera intentarlo o de recuperar la contraseña.
