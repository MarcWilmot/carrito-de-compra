## Anónimo
ss
Cómo anónimo podré:

+ Navegar por los productos aplicando diferentes filtros
+ Agregar una cantidad *X* de un determinado producto al carrito
+ Eliminar productos del carrito
+ Registrarse cómo Anónimo
---

### Navegar por los productos usando filtros
Como **Anónimo** quiero **aplicar filtros a los productos** para **ver fácil y directamente los productos que necesito**

#### Criterios de aceptación:
Dado **que no queda stock del producto ofertado** Cuando **el Anónimo navega por los productos** Entonces **mostrar un mensaje de AGOTADO**

Dado **que hay muchos productos** Cuando **el Anónimo filtra los productos** Entonces **mostrar una paginación**

---

### Añadir productos al carrito
Como **Anónimo** quiero **agregar más de un producto al carrito a la vez** para **comprar el mismo producto un número varias veces de forma rápida y simple**

#### Criterios de aceptación:
Dado **que el número de productos solicitados es mayor que el disponible en el stock** cuando **compro en la misma acción el mismo producto** entonces **mostrar un mensaje informándo que no puede comprar más de X veces ese producto**

Dado **que ya tenía el producto en el proceso de compra (carrito)** cuando **vuelve a comprar el mismo producto** entonces **aumentar en la vista de carrito el número del producto solicitado, no duplicar el producto**

---
### Eliminar productos del carrito
Como **Anónimo**  quiero **eliminar productos añadidos al carrito**  para **modificar mi compra final**
#### Criterios de aceptación:
Dado **que pudiera eliminar un producto por error** cuando **gestiono mi carrito de compra** entonces **mostrar un mensaje de confirmación**

---
### Procesar la compra
Como **Anónimo**  quiero **procesar la compra**  para **disponer de los productos comprados**.
#### Criterios de aceptación:
Dado **que puedo haberle dado a procesar la compra por error** cuando **gestiono mi carrito** entonces **mostrar un mensaje de confirmación**

---

### Login
Como **Anónimo**  quiero **loguearme**  para  **acceder al sistema**.
#### Criterios de aceptación:
Dado **que he olvidado la constraseña** cuando **intento acceder al sistema** entonces **ofrecerme la posibilidad de recuperar contraseña**

Dado **que he introducido la contraseña/usuario incorrecto** cuando **intento loguearme** entonces **mostrar un mensaje de error y volver a mostrar el formulario de logueo**

---

#### Criterios de aceptación:
+ **Dado** que el usuario Anónimo no dispone de todos los privilegios **cuando** intenta acceder a alguna funcionalidad que requiere previlegios avanzados, **entonces** informarle de que no puede acceder por falta de privilegios.
+ **Dado** que el usuario ha introduzido algún dato incorrecto **cuando** intenta logearse en el sistema **entonce** informarle de que algún dato es incorrecto y ofrecerle la oportunidad de volvera intentarlo o de recuperar la contraseña.

### Registro cómo Anónimo
Como **Anónimo**  quiero **poder registrarme**  para poder **acceder al sistema con todas las ventajas de los Anónimos**.

####Criterios de aceptación:
Dado **que el correo introducido** cuando **me estoy registrando, ya está en el sistema** entonces **informar de que ya es cliente y ofrecer la opción de recuperar contraseña**
Dado **que se tiene que verificar el correo** cuando **se finaliza el registro** entonces **mandar un e-mail de verificación con un código**

---
---

## Criterios de aceptación

+ **Dado** que el usuario anónimo no está registrado en el sistema **cuando** intenta acceder a alguna funcionalidad que requiere previlegios **entonces** informarle de que no puede acceder por falta de privilegios y redirigirle a la página de registro.
+ **Dado** que el usuario anónimo no puede procesar la compra **cuando** vaya a finalizar el proceso de compra **entonces** requerirle el registro cómo Anónimo.
+ **Dado** que el usuario ha introduzido algún dato incorrecto **cuando** intenta logearse en el sistema **entonce** informarle de que algún dato es incorrecto y ofrecerle la oportunidad de volvera intentarlo o de recuperar la contraseña.
