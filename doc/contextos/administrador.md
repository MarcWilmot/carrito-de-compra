## Administrador
Cómo administrador  podré:
ss
+ Ver el menú de adminsitración
+ Acceder al panel de administración
+ Consultar y procesar pedidos
+ Ver listado de cliente
+ Ver, añadir, modificar y eliminar clientes
+ Ver el historial de compra de cada cliente
+ Ver, agregar, modificar y eliminar productos
+ Enviar mails masivos a los clientes
+ Crear grupos de clientes
+ Catalogar los productos en distintas categorias
+ Recuperar constraseña

---

### Login
Como **Administrador**  quiero **loguearme**  para poder **acceder al sistema**.

#### Criterios de aceptación:
Dado **que he olvidado la constraseña** cuando **intento acceder al sistema** entonces **ofrecerme la posibilidad de recuperar contraseña**

Dado **que he introducido la contraseña/usuario incorrecto** cuando **intento loguearme** entonces **mostrar un mensaje de error y volver a mostrar el formulario de logueo**


---

#### Menú adminsitración
Como **Administrador**  quiero **ver el menú de adminsitración**  para poder **acceder al panel de adminsitración**.


---
#### Panel administración
Como **Administrador**  quiero **acceder al panel de adminsitración**  para poder **acceder a las funciones de adminsitración**.

---
#### Consultar Pedidos

Como **Administrador**  quiero **Consultar los pedidos pendientes realizados por los clientes**  para poder **poder verificar que el pago se ha realizado, y procesar el pedido.**.

#### Criterios de aceptación:
Dado **que pueden haber muchos pedidos** cuando **gestiono los pedidos** entonces **mostrar un filtro de ordenación por fecha**
---

#### Listado de clientes

Como **Administrador**  quiero **Ver un listado de los clientes**  para poder **saber cuantos clientes tenemos**.

---

### CRUD clientes

Como **Administrador**  quiero **Ver, modificar, y eliminar a clientes manualmente**  para poder **gestionar eficazmente a los clientes**.

#### Criterios de aceptación:
Dado **que puedo haber realizado una acción por error** cuando **gestiono a los clientes** entonces **mostrar un mensaje de confirmación**
---

### Historial clientes

Como **Administrador**  quiero **ver el historial de compra de cada cliente**  para poder **enviarle ofertas específicas a cada cliente relacionada con sus compras**.

---

### CRUD productos

Como **Administrador**  quiero **Ver, agregar, elimina y modificar productos en la tienda**  para poder **mantener actualizada y organizada la tienda**.

#### Criterios de aceptación:
Dado **que puedo haber realizado una acción por error** cuando **gestiono los productos** entonces **mostrar un mensaje de confirmación**

---

### Crear grupos de clientes

Como **Adminsitrador**  quiero **crear grupos de clientes**  para poder **organizar a los clientes por grupos**.

---

### Newsletter personalizados

Como **Administrador**  quiero **Enviar mails masivos a clientes o grupos de  clientes**  para poder **enviarles ofertas personalizadas**.

---

### Catalogar productos

Como **Administrador**  quiero **Catalogar los distintos productos**  para poder **facilitar la estructura de la tienda y la división lógica de los productos**.

---
### Recuperar contraseña
Como **Administrador**  quiero **recuperar mi contraseña**  para poder **acceder con mi al sistema si olvido la contraseña**.

#### Criterios de aceptación:
Dado **Que la contraseña nueva es la misma que la antigua** cuando **cambio mi contraseña** entonces **mostrar un mensaje de error y solicitar una nueva contraseña**

---
---

## Criterios de aceptación

+ **Dado** que el administrador no dispone de todos los privilegios **cuando** intenta acceder a alguna funcionalidad que requiere previlegios de superadminsitrador, **entonces** informarle de que no puede acceder por falta de privilegios.
+ **Dado** que el usuario ha introduzido algún dato incorrecto **cuando** intenta logearse en el sistema **entonce** informarle de que algún dato es incorrecto y ofrecerle la oportunidad de volvera intentarlo o de recuperar la contraseña.
