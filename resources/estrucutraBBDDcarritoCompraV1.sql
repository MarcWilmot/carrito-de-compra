DROP DATABASE  IF EXISTS `carritoCompra`;

CREATE DATABASE  IF NOT EXISTS `carritoCompra`;
USE `carritoCompra`;

--
-- Estructura de la tabla 'usuario'
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
	`id` int NOT NULL,
    `nombre` varchar(50) NOT NULL,
	`apellidos` varchar(100) NOT NULL,
    `dni-nif` varchar(50) NOT NULL,
    `email` varchar(100) NOT NULL,
    `pais` varchar(50) NOT NULL,
    `ciudad` varchar(50) NOT NULL,
    `direccion_fisica` varchar(150) NOT NULL,
    `numero_cuenta` varchar(150) NOT NULL,
    `codigo_postal` int NOT NULL,
	`usuario` varchar(50) NOT NULL,
	`password` varchar(50) NOT NULL,
    `fecha_alta` timestamp NOT NULL,
    `rol` varchar(50) NOT NULL,
	`habilitado` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Estructura de la tabla 'Producto'
--
DROP TABLE IF EXISTS `producto`;
CREATE TABLE `producto` (
	`id` int NOT NULL,
    `nombre` varchar(50) NOT NULL,
	`imagen_producto` varchar(150) NOT NULL,
    `categoria_producto` int NOT NULL,
	`stock` varchar(50) NOT NULL,
	`precio` varchar(50) NOT NULL,
	`habilitado` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Estructura de la tabla 'Producto-Usuario'
--
DROP TABLE IF EXISTS `producto-usuario`;
CREATE TABLE `producto-usuario` (
	`id` int NOT NULL,
   	`id_compra` int NOT NULL,
   	`id_producto` int NOT NULL,
   	`id_usuario` int NOT NULL,
    `fecha_compra` timestamp NOT NULL,
   	`procesado` tinyint(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Estructura de la tabla 'Categoria'
--
DROP TABLE IF EXISTS `categoria`;
CREATE TABLE `categoria` (
	`id` int NOT NULL,
	`nombre` varchar(50) NOT NULL,
	`imagen_categoria` varchar(150),
	`rol` varchar(50) NOT NULL,

  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Inserting data for table `users`
--

-- INSERT INTO `users` 
-- VALUES 
-- ('john','{noop}test123',1),
-- ('mary','{noop}test123',1),
-- ('susan','{noop}test123',1);


--
-- Estructura de la tabla 'Authorities' (Rol)
--

--  DROP TABLE IF EXISTS `authorities`;
-- CREATE TABLE `authorities` (
--  `username` varchar(50) NOT NULL,
--  `authority` varchar(50) NOT NULL,
--  UNIQUE KEY `authorities_idx_1` (`username`,`authority`),
--  CONSTRAINT `authorities_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Inserting data for table `authorities`
--

-- INSERT INTO `authorities` 
-- VALUES 
-- ('john','ROLE_EMPLOYEE'),
-- ('mary','ROLE_EMPLOYEE'),
-- ('mary','ROLE_MANAGER'),
-- ('susan','ROLE_EMPLOYEE'),
-- ('susan','ROLE_ADMIN');


